---
title: Sobre
permalink: /about/
---

# Sobre

A  paySmart  ajuda  Emissores  a  criarem  rapidamente  uma  operação  robusta  de  pagamentos eletrônicos com cartões físicos (com e sem contato), wearables e celulares, com EMV, NFC e QR  Codes.  A  paySmart  possui  APIs  simples  e  seguras  que  garantem  integração  em  poucos dias e um modelo comercial de baixo custo, baseado em software como serviço. 

Quer saber mais sobre nós? Visite nosso site <a href="https://paysmart.com.br/">paysmart.com.br</a>
    

