---
title: Códigos de Retorno
name: erros
tags:
    - pps
    - erros
description: Lista de possíveis códigos retornados pelo PPS.
type: pps
---
{% include erros_header.html %}

<table>
    <thead>
        <tr>
            <th>Código</th>
            <th>Mensagem</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>0</td>
            <td>Comando processado com sucesso</td>
        </tr>
        <tr>
            <td>1</td>
            <td>Parâmetro inválido – tamanho dos dados difere do esperado</td>
        </tr>
        <tr>
            <td>2</td>
            <td>Parâmetro obrigatório faltando na construção do pacote</td>
        </tr>
        <tr>
            <td>3</td>
            <td>Formato do comando inválido</td>
        </tr>
        <tr>
            <td>4</td>
            <td>Código de comando inválido</td>
        </tr>
        <tr>
            <td>5</td>
            <td>Parâmetro inválido - caracteres inválidos</td>
        </tr>
        <tr>
            <td>6</td>
            <td>Comando inválido</td>
        </tr>
        <tr>
            <td>7</td>
            <td>Número de parâmetros inválidos</td>
        </tr>
        <tr>
            <td>53</td>
            <td>Chave de transporte não encontrada</td>
        </tr>
        <tr>
            <td>55</td>
            <td>Falha na comunicação com o HSM</td>
        </tr>
        <tr>
            <td>56</td>
            <td>Falha na operação do HSM</td>
        </tr>
        <tr>
            <td>57</td>
            <td>Tempo de espera esgotado</td>
        </tr>
        <tr>
            <td>60</td>
            <td>Conteúdo inválido</td>
        </tr>
        <tr>
            <td>61</td>
            <td>Faltando dados obrigatórios</td>
        </tr>
        <tr>
            <td>62</td>
            <td>PINBlock inválido</td>
        </tr>
        <tr>
            <td>63</td>
            <td>KSN inválido</td>
        </tr>
        <tr>
            <td>64</td>
            <td>Tipos de dados não válido</td>
        </tr>
        <tr>
            <td>65</td>
            <td>Conversão de dados não válida</td>
        </tr>
        <tr>
            <td>66</td>
            <td>CVV inválido</td>
        </tr>
        <tr>
            <td>67</td>
            <td>Erro de autenticação de Key Block</td>
        </tr>
        <tr>
            <td>70</td>
            <td>PIN bloqueado</td>
        </tr>
        <tr>
            <td>71</td>
            <td>Verificação do PIN falhou</td>
        </tr>
        <tr>
            <td>72</td>
            <td>PIN Ausente</td>
        </tr>
        <tr>
            <td>73</td>
            <td>PIN Ausente</td>
        </tr>
        <tr>
            <td>74</td>
            <td>Senha não permitida</td>
        </tr>
        <tr>
            <td>80</td>
            <td>Erro na conexão com a base</td>
        </tr>
        <tr>
            <td>98</td>
            <td>Erro Interno</td>
        </tr>
        <tr>
            <td>99</td>
            <td>Erro Interno</td>
        </tr>
    </tbody>
</table>