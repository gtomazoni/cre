---
title: PPS
tags:
    - pps
description: "O PPS (PIN Protection Service) é um serviço paySmart que permite realizar validação e atualização de PIN, além de uma série de operações criptográficas com chaves 3DES nos modos DUKPT e WK."
name: "pps"
type: service
---
{% include service_header.html %}