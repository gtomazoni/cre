---
title: Validate
name: validate
tags:
    - pps
    - validate
    - endpoint
description: Endpoint para pedidos de validação de PIN Block.
type: pps
method: POST
---
{% include endpoint_header.html %}

### **Argumentos**

<table>
    <thead>
        <tr>
            <th>Nome</th>
            <th>Tipo</th>
            <th>Descrição</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>PAN</td>
            <td>String</td>
            <td>Valor entre 16 e 19 dígitos.</td>
        </tr>
        <tr>
            <td>PANSequenceNumber</td>
            <td>String</td>
            <td>Conteúdo do bit 23 (Card/PAN Sequence Number - 5F34). Usado para distinguir entre dois cartões com o meso PAN. Valor com 2 dígitos.</td>
        </tr>
        <tr>
            <td>PINBlock</td>
            <td>String</td>
            <td>PIN Block cifrado que deve ser usado para substituir o PIN armazenado na base para o cartão indicado. Valor em Hexadecimal.</td>
        </tr>
        <tr>
            <td>Format</td>
            <td>String</td>
            <td>Indica o formato do PIN Block. 00 para ISO-0 e 02 para ISO-2. É altamente recomendado que o formato ISO-0 seja utilizado. O suporte para ISO-2 é considerado deprecated. Se não estiver presente ISO-0 é assumido.</td>
        </tr>
        <tr>
            <td>KeyIdentifier</td>
            <td>String</td>
            <td>Valor de 4 dígitos hexadecimais usado para identificar qual chave de transporte está sendo usada.</td>
        </tr>
        <tr>
            <td>EchoField <b>[Opcional]</b></td>
            <td>String</td>
            <td>Campo opcional que será ecoado na resposta caso venha preenchido. Valor com 1 a 256 caracteres ASCII.</td>
        </tr>
    </tbody>
</table>

### **Exemplo de Requisição**

```
curl --key client.key --cert client.crt \
-X POST https://api.paysmart.com.br:60443/api/v1/pps/validate \
-H "Content-type: application/json" \
-H "x-api-key: 1e42fc40-9532-4269-9a03-7be0949ef0c7" \
"{
    "PAN": "1234567890123456",
    "PANSequenceNumber": "00",
    "PINBlock": "088DAC4CDB5DF987",
    "Format": "00",
    "KeyIdentifier": "ABCD",
    "EchoField": "ABCDFWG123"
}"
```

### **Exemplo de Resposta**

```json
{
    "resultCode": 0,
    "resultDescription": "Comando processado com sucesso",
    "psResponseId": "dfd88b95-d28d-49ac-90ba-7ba0b35aebd0",
    "tryCounter": 0,
    "echoField": "ABCDFWG123"
}
```
### **Descrição da Resposta**

<table>
    <thead>
        <tr>
            <th>Nome</th>
            <th>Tipo</th>
            <th>Descrição</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>resultCode</td>
            <td>Integer</td>
            <td>Código de resultado do processamento.</td>
        </tr>
        <tr>
            <td>resultDescription</td>
            <td>String</td>
            <td>Descrição textual do resultado do processamento.</td>
        </tr>
        <tr>
            <td>psResponseID</td>
            <td>String</td>
            <td>Identificador único da resposta. Gerado pela paySmart.</td>
        </tr>
        <tr>
            <td>tryCounter</td>
            <td>Integer</td>
            <td>Número de tentativas consecutivas com falha de validação. Assume o valor “0” em caso de sucesso.</td>
        </tr>
        <tr>
            <td>echoField <b>[Opcional]</b></td>
            <td>String</td>
            <td>Campo com o valor informado na requisição.</td>
        </tr>
    </tbody>
</table>

{% include endpoint_footer.html %}