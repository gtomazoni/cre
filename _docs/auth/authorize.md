---
title: Authorize
name: authorize
tags:
    - autorizador
    - authorize
    - endpoint
description: Endpoint para pedidos de validação do criptograma.
type: auth
method: POST
---
{% include endpoint_header.html %}

### **Argumentos**

<table>
    <thead>
        <tr>
            <th>Nome</th>
            <th>Tipo</th>
            <th>Descrição</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>Mode</td>
            <td>String</td>
            <td>00 para Partial Grade e 01 para Full Grade.</td>
        </tr>
        <tr>
            <td>ARC</td>
            <td>String</td>
            <td>00 para autorizar e 01 para negar a transação.</td>
        </tr>
        <tr>
            <td>CSUOptions</td>
            <td>String</td>
            <td>Utilizado apenas por emissores CCD. Vetor numérico representado em 8 dígitos.</td>
        </tr>
        <tr>
            <td>EMVData</td>
            <td>String</td>
            <td>Conteúdo do bit55. Tamanho variável. Valor em hexadecimal.</td>
        </tr>
        <tr>
            <td>PAN <b>[Opcional]</b></td>
            <td>String</td>
            <td>Conteúdo do bit2 (PAN - 5A). Opcional caso já esteja no bit55. Valor entre 16 e 19 dígitos.</td>
        </tr>
        <tr>
            <td>TransactionDate <b>[Opcional]</b></td>
            <td>String</td>
            <td>Data de realização da transação, no horário local do terminal, no formato AAMMDD. Este campo não é necessário se a data da transação já estiver presente nos dados EMV através do elemento “Transaction Date” (9A).</td>
        </tr>
        <tr>
            <td>TransactionCurrencyCode <b>[Opcional]</b></td>
            <td>String</td>
            <td>Conteúdo do bit49 (Transaction Currency Code - 5F2A). Opcional caso já esteja no bit55. Valor com 4 dígitos hexadecimais.</td>
        </tr>
        <tr>
            <td>AmountAuthorized <b>[Opcional]</b></td>
            <td>String</td>
            <td>Conteúdo do bit 4 (Amount, Authorized - 9F02). Opcional caso já esteja no bit55. Valor com 12 dígitos decimais.</td>
        </tr>
        <tr>
            <td>AmountOther <b>[Opcional]</b></td>
            <td>String</td>
            <td>Conteúdo do bit 54 (Amount, Other - 9F03). Default 000000000000 assumido caso não esteja presente nem aqui nem no bit55. Valor com 12 dígitos decimais.</td>
        </tr>
        <tr>
            <td>PANSequenceNumber <b>[Opcional]</b></td>
            <td>String</td>
            <td>Conteúdo do bit 23 (Card/PAN Sequence Number - 5F34). Default 00 assumido caso não esteja presente nem aqui nem no bit55. Valor com 2 dígitos hexadecimais.</td>
        </tr>
        <tr>
            <td>EchoField <b>[Opcional]</b></td>
            <td>String</td>
            <td>Campo opcional que será ecoado na resposta caso venha preenchido. Valor com 1 a 256 caracteres ASCII.</td>
        </tr>
    </tbody>
</table>

### **Exemplo de Requisição**

```
curl --key client.key --cert client.crt \
-X POST https://api.paysmart.com.br:60443/api/v1/auth/authorize \
-H "Content-type: application/json" \
-H "x-api-key: 1e42fc40-9532-4269-9a03-7be0949ef0c7" \
{
    "Mode": "01",
    "ARC": "00",
    "CSUOptions": "00000020",
    "EMVData": "5A0857042301020304055F340100820259008407AAA000000101039F02060000000001009F03060000000000009F1A020076950560800000005F2A0209869A031005149C01009F3704AFFB4A7A9F2701809F360200059F10200FA501A20800580000000000000000000F0000000000000000000000000000009F26084218EE98008AC74A",
    "EchoField": "ABCDFWG123"
}
```

### **Exemplo de Resposta**

```json
{
    "resultCode": 0,
    "resultDescription": "Comando processado com sucesso",
    "psResponseId": "dfd88b95-d28d-49ac-90ba-7ba0b35aebd0",
    "IAD": "9108ED26222A00820000",
    "echoField": "ABCDFWG123"
}
```

### **Descrição da Resposta**

<table>
    <thead>
        <tr>
            <th>Nome</th>
            <th>Tipo</th>
            <th>Descrição</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>resultCode</td>
            <td>Integer</td>
            <td>Código de resultado do processamento.</td>
        </tr>
        <tr>
            <td>resultDescription</td>
            <td>String</td>
            <td>Descrição textual do resultado do processamento.</td>
        </tr>
        <tr>
            <td>psResponseID</td>
            <td>String</td>
            <td>Identificador único da resposta. Gerado pela paySmart.</td>
        </tr>
        <tr>
            <td>IAD</td>
            <td>String</td>
            <td>Tag 91 contendo Issuer Authentication Data. [Opcional] Retorno obrigatório quando Mode do comando A1 for Full Grade (01) e ErrorCode retornar com sucesso (00). Valor em hexadecimal com tamanho variável.</td>
        </tr>
        <tr>
            <td>echoField <b>[Opcional]</b></td>
            <td>String</td>
            <td>Campo com o valor informado na requisição.</td>
        </tr>
    </tbody>
</table>

{% include endpoint_footer.html %}