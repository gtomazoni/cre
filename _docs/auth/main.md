---
title: Autorizador EMV
tags: 
    - autorizador
description: "Serviço que permite validar a autenticidade de transações EMV e gerar comandos administrativos (scripts) para controlar o ciclo de vida e o estado de aplicações EMV. "
name: "auth"
type: service
---
{% include service_header.html %}