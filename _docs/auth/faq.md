---
title: FAQ
name: faq
description: "Perguntas e Respostas Frequentes"
type: auth
---
{% include erros_header.html %}

**1) Além da interface REST existe alguma maneira de se comunicar com o paySmart EMV Authorization Server?**  
Sim, existe a interface TCP/IP (protocolo PSAuthProtocol) e outro produto paySmart (paySmart Gateway) que permite se conectar com o autorizador diretamente através de uma interface ISO8583.

**2) Qual valor deve ser devolvido no bit 55 de resposta, quando o criptograma é inválido (erro 50)?**  
Quando houver uma situação de criptograma inválido, ou qualquer outro, a transação deve ser negada pela processadora/emissor.

**3) Estamos inserindo um IAD "9100" e está aparecendo no terminal "transação negada pelo cartão".**  
Se aparece "Transação negada pelo cartão", muito provavelmente o terminal recebeu um pacote ISO8583 indicando que a transação foi autorizada pelo host mas o cartão não está conseguindo validar os Dados de Autenticação do Emissor.  
Em casos de "Criptograma Inválido", a transação não deve ser autorizada sob nenhuma hipótese, pois pode indicar uma tentativa de fraude.  
OBS: um cartão *full-grade* (*chip-grade* na nomenclatura MasterCard), não irá autorizar uma transação online sem os Dados de Autenticação do Emissor. 

**4) O terminal está solicitando as tags 9F02,9F03,9F1A,95,5F2A,9A,9C,9F37,9F35,9F45,9F34,9F27,9F36,9F26 e 9F10 e o bit 55 está sendo montado assim. Por que não consigo validar criptogramas (erro 61)?**  
Neste exemplo, estão faltando os elementos 84 (AID), 82 (AIP), 5A (PAN) e 5F34 (PANSeq). Lembrando que PAN e PAN Sequence podem ser passados também diretamente por parâmetro (recomendado)

**5) Estou implementando uma validação de PTC e PTL e estou com dúvidas em relação ao CVR. O CVR informa que o PTC está excedido e o TVR não. Em qual valor devo me basear para realizar a validação?**  
Idealmente deve-se basear no CVR já que esse dado é gerado internamente pelo cartão e não pode ser manipulado por oponentes em um terminal comprometido.

**6) Enviamos duas mensagens de autorização idênticas, com exatamente os mesmos dados e aconteceu o seguinte: em ambiente de testes, tudo funcionou. Em ambiente de produção, a primeira retornou sucesso (00) e a segunda, exatamente igual, retornou erro 97 (condições de segurança não satisfeitas). Por que?! O que fazer?**  
O comportamento está correto. Duas transações idênticas indicam um ataque de *replay*. Essa validação está desabilitada para facilitar os testes iniciais de novos emissores em ambiente de desenvolvimento, mas habilitada em produção.

**7) Estou fazendo testes e percebi que cartões emitidos com o mesmo PAN e datas de expiração diferente estão retornando erro 97 (condições de segurança não satisfeitas). Não é possível ter dois PANs iguais com datas diferentes?**  
Sim, é possível, mas para isso é preciso emitir os cartões com sequenciadores do PAN (*PAN Sequence Number*) distintos.  
Uma outra alternativa seria desligar a funcionalidade de validação de ataques de *replay*, mas isso não é recomendado pela paySmart.

**8) O PAN Sequence Number (PSN, PANSeq) vem na área EMV ou no bit 23?**  
Isso pode variar de adquirente para adquirente.  
Na Cielo, por exemplo, vem indicado pelo *tag* 5F34 na área EMV (bit 55). Na GetNet, por outro lado, vem no bit 23. 

**9) Em uma transação full-grade, os Dados de Autenticação do Emissor (Issuer Authentication Data), retornados pelo autorizador, são enviados ao Terminal que os envia para cartão. O que acontece se o cartão rejeitar esse dado, isto é, falhar a validação deste criptograma do emissor? O que a rede de captura faz com esse segundo criptograma? É enviado em algum arquivo de conciliação, por exemplo? Há alguma necessidade do emissor em validar esse criptograma?**  
Sim, em alguns casos (embora muito raros), um cartão *full-grade* pode negar uma transação aprovada *online*. Isso pode ocorrer por corrupção do pacote de resposta (embora a maioria dos terminais tenha métodos para prevenir essas falhas) ou ataques de homem-do-meio, onde um oponente tenha manipulado o retorno ao cartão. Nesses casos, o que o terminal costuma fazer é enviar o criptograma de negação, gerado pelo cartão, na próxima transação (desfazimento), mas isso varia de adquirente para adquirente. Importante observar que se o cartão negar a transação o terminal mostrará "TRANSAÇÃO NEGADA PELO CARTÃO" e não imprimirá comprovante, etc. 

**10) Já que alguns adquirentes (por exemplo, a Cielo) não enviam o AID (tag 84) nos dados EMV, como posso saber se estou tratando com uma aplicação de débito ou crédito para poder informar adequadamente o autorizador paySmart?**  
Realmente, o fato de alguns adquirentes não enviarem o AID selecionado pelo terminal dificulta um pouco o trabalho de emissores, principalmente emissores de cartões múltiplos.
Emissores paySmart têm se baseado das seguintes soluções de contorno:
* Uma tabela DE-PARA de "BINs de 8 dígitos" e AIDs no caso de cartões monoaplicação
(ex: benefícios PAT)
* Utilizar o código de processamento (bit 03) para diferenciar débito e crédito
* Utilizar parte do PANSequence para diferenciar entre aplicações

**11) Quais formatos de criptograma são suportados pelo autorizador paySmart?**  
* Common Payment Application / Common Core Definitions (CCD) versão de
criptograma A5 - aplicações paySmart
* Visa VSDC
* MasterCard M/Chip  
<b>OBS:</b> O suporte à criptogramas MasterCard e Visa é opcional e requer licenças
específicas.

**12) AID e IAD significam a mesma coisa?**  
Não. AID é um acrônimo para *Application IDentifier* (Identificador de Aplicações),
definido pela ISO7816 e IAD é um acrônimo para *Issuer Authentication Data* (Dados de
Autenticação do Emissor).  
AIDs são elementos de 5 a 11 bytes (representados como strings de 10 a 22 caracteres
hexadecimais) que, comumente, têm 14 caracteres (7 bytes). Nos dados EMV, AIDs normalmente estão dentro de tags 84.  
Exemplo: "8407A0000000031010" (Nesse exemplo, o AID é o "A0 00 00 00 03 10 10" )  
IADs são retornados por Emissores EMV e são encapsulados dentro de tags 91.  
Exemplo: 91080ECE147C00820000 (no exemplo, o valor do IAD é "0E CE 14 7C 00 82 00 00")

**13) Se eu for negar uma transação (por exemplo, por saldo insuficiente) o que devo fazer? Preciso envolver o autorizador paySmart? Por que não simplesmente mandar vazio?**  
Sim, em cartões *“full-grade”*, o ideal é sempre envolver o autorizador. Assim, pode-se gerar um código de resposta que irá proteger o conteúdo da mensagem de autorização (no caso, "negar a transação") mas que é diferente de um pacote recebido sem IAD.  
Para negar uma transação, basta enviar uma requisição com o valor 01 no campo do **ARC**.

