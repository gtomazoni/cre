---
title: "Acesso à API"
name: acesso
description: "Instruções de acesso ao CRE API"
tags: 
    - cre api
    - acesso
---
{% include erros_header.html %}

### **Endpoints**

* **Homologação:** <a href="https://cre-hml.paysmart.com.br:60443/api/v1/">https://cre-hml.paysmart.com.br:60443/api/v1/</a>
* **Produção:** <a href="https://cre.paysmart.com.br:60443/api/v1/">https://cre.paysmart.com.br:60443/api/v1/</a>

### **Acesso**

O acesso a API é feito por um canal HTTPS com autenticação mútua. O certificado apresentado pela paySmart na API é emitido por uma autoridade certificadora confiável, não necessitando de configuração adicional no lado do solicitante. Já o certificado do cliente consiste em um par composto por:

* Uma chave privada RSA (com mínimo 2048 bits) emitida pelo próprio cliente, para a qual deve gerada uma requisição de assinatura do tipo CSR a ser enviada para a paySmart.
* Um certificado público assinado pela paySmart, utilizando o CSR fornecido.

No ambiente de homologação, a paySmart poderá fornecer a chave privada e o certificado público para acesso ao respectivo endpoint.

Nas requisições HTTP, é necessário o envio da **API-Key**, que é uma chave hexadecimal de 20 bytes (40 caracteres) que deve ser enviada no cabeçalho da requisição HTTP, com o nome de **x-api-key**, identificando o emissor. É criada pela paySmart e fornecida ao emissor, que deve enviá-la em todas as requisições, a fim de ser identificado de maneira lógica pela API.