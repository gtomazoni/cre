---
title: CRE API v1
tags: 
 - cre api
 - geral
description: Funcionamento geral do CRE
permalink: /
---

# {{page.title}}

O CRE consiste em uma API REST para oferecer acesso aos serviços de autorização e PIN. Segue abaixo as operações suportadas por cada serviço.

{% assign services=site.docs | where: "type", "service" %}
{% for service in services %}
## **{{service.title}}**
{{service.description}}
<ul>
    {% assign endpoints=site.docs | where: "type", {{service.name}} %}
    {% for endpoint in endpoints %}
    <li><a href="./docs/{{service.name}}/{{endpoint.name}}">
        {% if endpoint.tags contains "endpoint" %}{{endpoint.method}} /{{service.name}}/{{endpoint.name}}{% else %}{{endpoint.title}}{% endif %}
    </a></li>
    {% endfor %}
</ul>
{% endfor %}
